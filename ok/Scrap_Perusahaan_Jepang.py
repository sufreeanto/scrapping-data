
# coding: utf-8

# In[1]:


from urllib.request import urlopen, Request
keyword = "120"
headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.3'}
#reg_url = "https://en.perusahaanjepang.com/company/?paged1="+keyword
reg_url = "https://en.perusahaanjepang.com/company/"
req = Request(url=reg_url, headers=headers) 
html = urlopen(req).read() 
print(html[1:1000]) 


# In[2]:


from bs4 import BeautifulSoup
soup = BeautifulSoup(html, "lxml")
print(type(soup))
print(soup.prettify()[1:1000])


# In[3]:


produk = soup.find_all("article")
i = 1
for p in produk:
    print (p.find('h3', 'hthree-comp-list').get_text())
    print (p.find('p', '').get_text())
    print (p.find('h4', '').get_text())
    print ("-----")


# In[4]:


from IPython.display import display, HTML
import pandas as pd

nama_company = []
nama_alamat = []
no_telpun = []

produk = soup.find_all("article")

for p in produk:
    namaper = p.find('h3', 'hthree-comp-list').get_text().encode('utf-8')
    namaal = p.find('p', '').get_text().encode('utf-8')
    notelp = p.find('h4', '').get_text().encode('utf-8')
    if namaper != "JMAX":
        nama_company.append(namaper)
        nama_alamat.append(namaal)
        no_telpun.append(notelp)

produk_dict = {'namaper':nama_company, 'namaal':nama_alamat, 'notelp':no_telpun}

df = pd.DataFrame(produk_dict, columns=['namaper','namaal','notelp'])

#df.sort_values('harga', ascending=True)


# In[5]:


df.to_csv("perusahaanjepang11111"+keyword+".csv")

